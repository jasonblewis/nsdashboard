#!/usr/bin/env perl
use Mojo::URL;
use 5.16.0;

# Parse
my $urlstr = 'http://sri:foo@example.com:3000/foo/84324?bar=baz#23';
my $url = Mojo::URL->new($urlstr);
say $urlstr;
say $url->scheme;
say $url->userinfo;
say $url->host;
say $url->port;
say $url->path;
say $url->query;
say $url->fragment;
say "get the last bit of the url:";
say $url->path->parts->[-1];

# Build
my $url1 = Mojo::URL->new;
$url1->scheme('http');
$url1->host('example.com');
$url1->port(3000);
$url1->path('/foo/bar');
$url1->query(foo => 'bar');
$url1->fragment(23);
say "$url1";
