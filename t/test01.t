use Test::More;
use Mojo::File qw(curfile);
use Test::Mojo;

# Portably point to "../dash.pl"
my $script = curfile->dirname->sibling('dash.pl');

my $t = Test::Mojo->new($script);

# HTML/XML
$t->get_ok('/welcome')->status_is(200)->text_is('div#message' => 'welcome');

# JSON
$t->get_ok('/users/list')
  ->status_is(200)
  ->header_is('Server' => 'Mojolicious (Perl)')
  ->header_isnt('X-Bender' => 'Bite my shiny metal ass!')
  ->json_is({users => [ {firstname => "User1",  lastname => "User1 Last name" },
                            {firstname => "User 2", lastname => "User 2 last name" },
                      ]
             })
    ;

# # WebSocket
# $t->websocket_ok('/echo')
#   ->send_ok('hello')
#   ->message_ok
#   ->message_is('echo: hello')
#   ->finish_ok;

done_testing();
