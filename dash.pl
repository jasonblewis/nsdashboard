#!/usr/bin/env perl
# Automatically enables "strict", "warnings", "utf8" and Perl 5.16 features
use 5.16.0;

use Mojolicious::Lite -signatures;
use Mojo::UserAgent;
use Mojo::URL;

use YAML qw/LoadFile/;
use FindBin;
use Path::Tiny;

use WWW::OAuth;
use Digest::SHA 'hmac_sha256_base64';
use URI::Escape;
use Data::Dumper;

use Test::JSON;

my $config;

#say Dumper $ENV{'CONFIG'} ;

if(defined $ENV{'CONFIG'}) {
    $config = $ENV{'CONFIG'}
} else {
    $config = 'conf.yaml'
}

my $conf = path($FindBin::Bin, $config);

die "$conf is not a file" unless -f $conf;
my $settings = LoadFile($conf);
my $globals = delete $settings->{global} || {};


my $client_id     ||= $globals->{client_id};
my $client_secret ||= $globals->{client_secret};
my $token         ||= $globals->{token};
my $token_secret  ||= $globals->{token_secret};
my $realm         ||= $globals->{realm};
my $site          ||= $globals->{site};
my $secret_phrase ||= $globals->{secret_phrase};
my $invoice_base_url ||= $globals->{invoice_base_url};
my $rest_services_base    ||= $globals->{rest_services_base};
my $sales_order_url ||= $globals->{sales_order_url};
my $inventory_item_url ||= $globals->{inventory_item_url};
my $item_url ||= $globals->{item_url};
my $notesbaseurl ||= $globals->{notes_url};

say "client_id: $client_id";
say "client_secret: $client_secret";
say "token: $token";
say "token_secret: $token_secret";
say "realm: $realm";
say "site: $site";
say "rest_services_base: $rest_services_base";

app->secrets([$secret_phrase]);

my $rest_services = Mojo::URL->new($rest_services_base);

say "rest_services: ", Dumper $rest_services;

my $oauth = WWW::OAuth->new(
  client_id => $client_id,
  client_secret => $client_secret,
  token => $token,
  token_secret => $token_secret,
  signature_method => 'HMAC-SHA256',
  signer => sub {
      # some HMAC-SHA256 stuff
      # copied from https://metacpan.org/dist/WWW-OAuth/source/lib/WWW/OAuth.pm#L94-101
        my ($base_str, $client_secret, $token_secret) = @_;
        $token_secret = '' unless defined $token_secret;
        my $signing_key = uri_escape_utf8($client_secret) . '&' . uri_escape_utf8($token_secret);
        my $digest = hmac_sha256_base64($base_str, $signing_key);
        $digest .= '='x(4 - length($digest) % 4) if length($digest) % 4; # Digest::SHA does not pad Base64 digests
        return $digest;
        }
);

my $ua  = Mojo::UserAgent->new;
$ua->on(start => sub {
            $oauth->authenticate($_[1]->req,{realm=>$realm});
        });

#say Dumper($oauth);





plugin 'AutoReload';

options '*' => sub {
    my $self = shift;

    $self->res->headers->header('Access-Control-Allow-Origin' => '*');
    #$self->res->headers->header('Access-Control-Allow-Credentials' => 'true');
    #$self->res->headers->header('Access-Control-Allow-Methods' => 'GET, OPTIONS, POST, DELETE, PUT');
    #$self->res->headers->header('Access-Control-Allow-Headers' => 'Content-Type');
    #$self->res->headers->header('Access-Control-Max-Age' => '1728000');

    $self->respond_to(any => { data => '', status => 200 });
};

app->hook(after_dispatch => sub {
    my $c = shift;
    $c->res->headers->header('Access-Control-Allow-Origin' => '*');
          });

# Non-blocking
get '/title' => sub ($c) {
  $c->ua->get('mojolicious.org' => sub ($ua, $tx) {
    $c->render(data => $tx->result->dom->at('title')->text);
  });
};


# Concurrent non-blocking
get '/titles' => sub ($c) {
  my $mojo = $c->ua->get_p('https://mojolicious.org');
  my $cpan = $c->ua->get_p('https://metacpan.org');
  Mojo::Promise->all($mojo, $cpan)->then(sub ($mojo, $cpan) {
    $c->render(json => {
      mojo => $mojo->[0]->result->dom->at('title')->text,
      cpan => $cpan->[0]->result->dom->at('title')->text
    });
  })->wait;
};


get '/salesOrder' => sub($c) {
    say "Getting salesOrder: ";
    my $salesOrderurl = Mojo::URL->new($rest_services)->path($sales_order_url)->to_abs;
#    say "sales order: ", Dumper($salesOrderurl);

#    say "salesOrderurl: $salesOrderurl";
#    say "sostr:     $sostr";
    my $res = $ua->get($salesOrderurl)->result;
#    my $res = $ua->get($salesOrderurl)->result;

    if    ($res->is_success)  { say "Success: ", $res->body }
    elsif ($res->is_error)    { say "Error: ", $res->message }
    elsif ($res->code == 301) { say "301: ", $res->headers->location }
    else                      { say 'Whatever...' };
    # say Dumper($res->json);
    # return ($res->json);
    return $c->render(
        template => $c->stash('path'),
        format => 'json',
        json => $res->json,
        );
};

get '/salesOrder/:id' => sub ($c) {
    my $id = $c->stash('id');
    my $salesOrderurl = Mojo::URL->new($rest_services)->path($sales_order_url)->path($id);
#    my $salesOrderurl = Mojo::URL->new($salesOrderbaseurl)->path($id);
    $salesOrderurl->query({expandSubResources => 'true'});
    say "salesOrderurl: $salesOrderurl";

    my $res = $ua->get($salesOrderurl)->result;

    if    ($res->is_success)  { say "Success: " }
    elsif ($res->is_error)    { say "Error: ", $res->message }
    elsif ($res->code == 301) { say "301: ", $res->headers->location }
    else                      { say 'Whatever...' };

    return $c->render(
        template => 'salesOrder',
        format => 'json',
        json => $res->json,
        );
};

# create a new stand alone sales order
# https://docs.oracle.com/en/cloud/saas/netsuite/ns-online-help/section_159793606645.html
post '/salesOrder' =>sub ($c) {
#    say '$c-req: ', Dumper $c->content;
    my $data = $c->req->json;
#    say "post data: ", Dumper $data;

    my $salesOrderurl = Mojo::URL->new($rest_services)->path($sales_order_url);
    my $res = $ua->post($salesOrderurl => {Accept => '*/*'} => json => $data )->result; 

    if    ($res->is_empty) { #1xx 204 or 304
        say "response emtpy - record was inserted: ", $res->headers->location;
        say "SO id: ", Mojo::URL->new($res->headers->location)->path->parts->[-1];
    }
    elsif ($res->is_success)  { say $res->body } #2xx
    elsif ($res->is_error)    { say $res->message } #4xx or 5xx
    elsif ($res->code == 301) { say $res->headers->location } 
    else                      { say 'Whatever...' }

    
    return $c->render(json => $data);
};


my $invoicebaseurl = Mojo::URL->new($invoice_base_url);
#my $invoiceurl = Mojo::URL->new($invoicebaseurl)->path('teststring');

#say "invoiceurl: $invoiceurl";



get '/swagger' => sub($c) {
    # use this to get the filterable fields for the q parameter.
    # see the header x-ns-filterable
    # https://www.reddit.com/r/Netsuite/comments/lkv0yd/rest_api_identifying_query_paramaters/
    
    say "getting swagger: ";
    my $swaggerurl = Mojo::URL->new($globals->{swagger_url});
    my $swaggerheader = Mojo::URL->new($globals->{swagger_header});
    #    $swaggerurl->query({expandSubResources => 'true',q => 'createdDate ON_OR_AFTER "5/03/2023"'});
    say "swaggerurl: $swaggerurl";
    say "swaggerheader: $swaggerheader";
    my $res = $ua->get($swaggerurl => {Accept => 'application/swagger+json'})->result;
#    my $res = $ua->get($swaggerurl)->result;

    if    ($res->is_success)  { say "Success: ", $res->body }
    elsif ($res->is_error)    { say "Error: ", $res->message }
    elsif ($res->code == 301) { say "301: ", $res->headers->location }
    else                      { say 'Whatever...' };
    # say Dumper($res->json);
    # return ($res->json);
    return $c->render(
        template => $c->stash('path'),
        format => 'json',
        json => $res->json,
        );
};
    
get '/invoice' => sub($c) {
    say "Getting invoice: ";
    my $invoiceurl = Mojo::URL->new($invoicebaseurl);
    $invoiceurl->query({q => 'createdDate ON_OR_AFTER "5/02/2023"'});
#    $invoiceurl->query({expandSubResources => 'true',q => 'createdDate ON_OR_AFTER "5/03/2023"'});
    say "invoiceurl: $invoiceurl";
    my $res = $ua->get($invoiceurl)->result;
#    my $res = $ua->get($invoiceurl)->result;

    if    ($res->is_success)  { say "Success: ", $res->body }
    elsif ($res->is_error)    { say "Error: ", $res->message }
    elsif ($res->code == 301) { say "301: ", $res->headers->location }
    else                      { say 'Whatever...' };
    # say Dumper($res->json);
    # return ($res->json);
    return $c->render(
        template => $c->stash('path'),
        format => 'json',
        json => $res->json,
        );
};

get '/customer' => sub($c) {
    say "Getting customers: ";
    my $customerurl = Mojo::URL->new($globals->{customer_url});
#    $customerurl->query({q => 'createdDate ON_OR_AFTER "5/02/2023"'});
#    $customerurl->query({expandSubResources => 'true',q => 'createdDate ON_OR_AFTER "5/03/2023"'});
    say "customerurl: $customerurl";
    my $res = $ua->get($customerurl)->result;
#    my $res = $ua->get($customerurl)->result;

    if    ($res->is_success)  { say "Success: ", $res->body }
    elsif ($res->is_error)    { say "Error: ", $res->message }
    elsif ($res->code == 301) { say "301: ", $res->headers->location }
    else                      { say 'Whatever...' };
    # say Dumper($res->json);
    # return ($res->json);
    return $c->render(
        template => $c->stash('path'),
        format => 'json',
        json => $res->json,
        );
};

get '/customer/:id' => sub ($c) {
    my $id = $c->stash('id');
    my $customerurl = Mojo::URL->new($globals->{customer_url})->path($id);
    $customerurl->query({expandSubResources => 'true'});
    say "customerurl: $customerurl";

    my $res = $ua->get($customerurl)->result;

    if    ($res->is_success)  { say "Success: " }
    elsif ($res->is_error)    { say "Error: ", $res->message }
    elsif ($res->code == 301) { say "301: ", $res->headers->location }
    else                      { say 'Whatever...' };


    say Dumper($res->json);
    # return ($res->json);

    return $c->render(
        template => 'customer',
        format => 'json',
        json => $res->json,
        );


};
 


get '/invoice/:id' => sub ($c) {
    my $id = $c->stash('id');
    my $invoiceurl = Mojo::URL->new($invoicebaseurl)->path($id);
    $invoiceurl->query({expandSubResources => 'true'});
    say "invoiceurl: $invoiceurl";

    my $res = $ua->get($invoiceurl)->result;

    if    ($res->is_success)  { say "Success: " }
    elsif ($res->is_error)    { say "Error: ", $res->message }
    elsif ($res->code == 301) { say "301: ", $res->headers->location }
    else                      { say 'Whatever...' };


    # say Dumper($res->json);
    # return ($res->json);

    return $c->render(
        template => 'invoice',
        format => 'json',
        json => $res->json,
        );


};

post '/note/:customer' => sub ($c) {
# create a new note for a given customer
    my $customer = $c->stash('customer');
#    my $notesurl = Mojo::URL->new($notesbaseurl)->path($customer);
    my $notesurl = Mojo::URL->new($notesbaseurl);
#    $notesurl->query({expandSubResources => 'true'});
#    $notesurl->query({q => 'entity EQUAL 1975'});
    say "notesurl: $notesurl";

    my $res = $ua->post($notesurl => {} => json => {entity => $customer, author => 8, note => 'test from perl as natasha'})->result;

    if    ($res->is_success)  { say "Success: " }
    elsif ($res->is_error)    { say "Error: ", $res->message }
    elsif ($res->code == 301) { say "301: ", $res->headers->location }
    else                      { say 'Whatever...' };


    # say Dumper($res->json);
    # return ($res->json);

    return $c->render(
        template => 'notes',
        format => 'json',
        json => $res->json,
        );



};

get '/notes/:customer' => sub ($c) {
# get notes from ns
    my $customer = $c->stash('customer');
#    my $notesurl = Mojo::URL->new($notesbaseurl)->path($customer);
    my $notesurl = Mojo::URL->new($notesbaseurl);
#    $notesurl->query({expandSubResources => 'true'});
    $notesurl->query({q => "entity EQUAL $customer"});
    say "notesurl: $notesurl";

    my $res = $ua->get($notesurl)->result;

    if    ($res->is_success)  { say "Success: " }
    elsif ($res->is_error)    { say "Error: ", $res->message }
    elsif ($res->code == 301) { say "301: ", $res->headers->location }
    else                      { say 'Whatever...' };


    # say Dumper($res->json);
    # return ($res->json);

    return $c->render(
        template => 'notes',
        format => 'json',
        json => $res->json,
        );


};

get '/note/:id' => sub ($c) {
# get note given note id from ns
    my $id = $c->stash('id');
    my $noteurl = Mojo::URL->new($notesbaseurl)->path($id);
#    my $noteurl = Mojo::URL->new($notesbaseurl);
    $noteurl->query({expandSubResources => 'true'});
#    $noteurl->query({q => "id EQUAL $id"});
    say "noteurl: $noteurl";

    my $res = $ua->get($noteurl)->result;

    if    ($res->is_success)  { say "Success: " }
    elsif ($res->is_error)    { say "Error: ", $res->message }
    elsif ($res->code == 301) { say "301: ", $res->headers->location }
    else                      { say 'Whatever...' };


    say Dumper("note: $id:", $res->json);
    # return ($res->json);

    return $c->render(
        template => 'note',
        format => 'json',
        json => $res->json,
        );

};

# i gave this a really good crack but ns gives an error when i try this: 'Please enter value(s) for: Memo.' after much searching i could find out nothing about this so i gave up

# get '/note_q/search' => sub ($c) {
#     # get notes matching an entity id
#     say "searching for notes using suiteql";
# #    my $my_notes_url = Mojo::URL->new($rest_services)->path($globals->{notes_url})->to_abs;
#     my $my_notes_url = Mojo::URL->new($globals->{notes_url});
#     say Dumper "notes url: ",  $my_notes_url;
#     my $res = $ua->post($my_notes_url => {Prefer => 'transient'} => json => { q =>
#        'select top 1 * from note where entity = 10274' # where n.customer = 10274
#                         })->result;


#     if    ($res->is_success)  { say "Success: ", $res->body }
#     elsif ($res->is_error)    { say "Error: ", $res->message }
#     elsif ($res->code == 301) { say "301: ", $res->headers->location }
#     else                      { say 'Whatever...' };
#     return $c->render(
#         template => 'note',
#         format => 'json',
#         json => $res->json,
#         );
# };




get '/inventoryItem' => sub($c) {
    say "Getting inventoryItem: ";
    my $inventoryItemURL = Mojo::URL->new($rest_services)->path($inventory_item_url)->to_abs;

        my $res = $ua->get($inventoryItemURL)->result;
#    my $res = $ua->get($salesOrderurl)->result;

    if    ($res->is_success)  { say "Success: ", $res->body }
    elsif ($res->is_error)    { say "Error: ", $res->message }
    elsif ($res->code == 301) { say "301: ", $res->headers->location }
    else                      { say 'Whatever...' };
    # say Dumper($res->json);
    # return ($res->json);
    return $c->render(
        template => $c->stash('path'),
        format => 'json',
        json => $res->json,
        );

};

get '/inventoryItem/:id' => sub ($c) {
    my $id = $c->stash('id');
    my $inventoryItemURL = Mojo::URL->new($rest_services)->path($inventory_item_url)->path($id);
#    my $inventoryitemurl = Mojo::URL->new($inventoryitembaseurl)->path($id);
#    $inventoryItemURL->query({expandSubResources => 'true'});
    say "inventoryItemURL: $inventoryItemURL";

    my $res = $ua->get($inventoryItemURL)->result;

    if    ($res->is_success)  { say "Success: " }
    elsif ($res->is_error)    { say "Error: ", $res->message }
    elsif ($res->code == 301) { say "301: ", $res->headers->location }
    else                      { say 'Whatever...' };

    return $c->render(
        template => 'inventoryitem',
        format => 'json',
        json => $res->json,
        );
};


get '/item' => sub ($c) {
    say "getting item using suiteql";
    my $itemURL = Mojo::URL->new($rest_services)->path($item_url)->to_abs;
    say Dumper $itemURL;
    my $res = $ua->post($itemURL => {Prefer => 'transient'} => json => { q =>
                                                                             #     "select i.id, i.externalId, i.itemid, i.itemType, i.upcCode, i.custitem4, i.custitem_outer_barcode, i.description, i.displayName, i.fullName, c.name, i.class from Item i inner join Class c on (i.class = c.id) where i.itemType != 'InvtPart' and i.isInactive = 'F'"
#"select Item.id, Item.class, C.name as Class from Item join classification c on Item.class = c.id "
#"select id, name from classification"
"select i.id, i.description, i.fullName, i.itemType, i.upcCode, c.name from item i join classification c on i.class = c.id"
                        })->result;


    if    ($res->is_success)  { say "Success: ", $res->body }
    elsif ($res->is_error)    { say "Error: ", $res->message }
    elsif ($res->code == 301) { say "301: ", $res->headers->location }
    else                      { say 'Whatever...' };
    return $c->render(
        template => $c->stash('path'),
        format => 'json',
        json => $res->json,
        );
};

get '/item/search' => sub ($c) {
    # get items matching search criteria WIP
    say "searching for an item using suiteql";
    my $itemURL = Mojo::URL->new($rest_services)->path($item_url)->to_abs;
    say Dumper $itemURL;
    my $res = $ua->post($itemURL => {Prefer => 'transient'} => json => { q =>
                                                                             #     "select i.id, i.externalId, i.itemid, i.itemType, i.upcCode, i.custitem4, i.custitem_outer_barcode, i.description, i.displayName, i.fullName, c.name, i.class from Item i inner join Class c on (i.class = c.id) where i.itemType != 'InvtPart' and i.isInactive = 'F'"
#"select Item.id, Item.class, C.name as Class from Item join classification c on Item.class = c.id "
#"select id, name from classification"
"select i.id, i.description, i.fullName, i.itemType, i.upcCode, c.name from item i join classification c on i.class = c.id"
                        })->result;


    if    ($res->is_success)  { say "Success: ", $res->body }
    elsif ($res->is_error)    { say "Error: ", $res->message }
    elsif ($res->code == 301) { say "301: ", $res->headers->location }
    else                      { say 'Whatever...' };
    return $c->render(
        template => $c->stash('path'),
        format => 'json',
        json => $res->json,
        );
};



get '/api' => sub($c) {
    my $mojo = $c->ua->get_p($site);
    Mojo::Promise->all($mojo)->then(sub ($mojo) {
                                               $c->render(json => {
                                                   mojo => $mojo->[0]->result->dom->at('title')->text,
                                               });
  })->wait;
};


get '/' => sub {
    my $self = shift;
    $self->render('baz',);
};

# Route with placeholder / returns Hello from <anything>
get '/:foo' => sub ($c) {
  my $foo = $c->param('foo');
  $c->render(text => "Hello from <div id='message'>$foo</div>.");
};

get '/users/list' => sub($c) {
    $c->render( json =>
                {users => [ {firstname => "User1",  lastname => "User1 Last name" },
                            {firstname => "User 2", lastname => "User 2 last name" },
                          ]
             }
              );

};

get '/users/list2' => sub($c) {
    $c->render( json =>
                {users => [ {firstname => "User1",  lastname => "User1 Last name" },
                            {firstname => "User 2", lastname => "User 2 last name" },
                          ]
             }
              );

};






# Start the Mojolicious command system
app->start;
__DATA__

@@ index.html.ep
    Hello world!

@@ baz.html.ep
    This is the Index page

@@ titles.html.ep
    this is the titles page

