# Netsuite API 

https://gitlab.com/jasonblewis/nsdashboard

see also ui for accessing this project

https://gitlab.com/jasonblewis/netsuite-ninja


sample project to learn how to:

1. access NetSuite API
2. learn svelte and mojolicious


Files:

mua.pl: example of using mojo useragent to query NetSuite API
dash.pl: example mojo app


to run this project:

with production NetSuite:

morbo ./dash.pl

with SandBox

CONFIG=sb-config.yaml morbo ./dash.pl

or 

CONFIG=sb-config.yaml ./mua.pl

