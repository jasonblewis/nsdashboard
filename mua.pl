#!/usr/bin/env perl
use Mojo::UserAgent;
use YAML qw/LoadFile/;
use FindBin;
use Path::Tiny;
use JSON;

use WWW::OAuth;
use Digest::SHA 'hmac_sha256_base64';
use URI::Escape;
use Data::Dumper;

use 5.16.0;

my $config;
if(defined $ENV{'CONFIG'}) {
    $config = $ENV{'CONFIG'}
} else {
    $config = 'conf.yaml';
}


my $conf = path($FindBin::Bin, $config);

die "$conf is not a file" unless -f $conf;
my $settings = LoadFile($conf);
my $globals = delete $settings->{global} || {};


my $client_id     ||= $globals->{client_id};
my $client_secret ||= $globals->{client_secret};
my $token         ||= $globals->{token};
my $token_secret  ||= $globals->{token_secret};
my $realm         ||= $globals->{realm};
my $site          ||= $globals->{site};
my $invoice_base_url ||= $globals->{invoice_base_url};

say "client_id: $client_id";
say "client_secret: $client_secret";
say "token: $token";
say "token_secret: $token_secret";
say "realm: $realm";
say "site: $site";

my $oauth = WWW::OAuth->new(
  client_id => $client_id,
  client_secret => $client_secret,
  token => $token,
  token_secret => $token_secret,
  signature_method => 'HMAC-SHA256',
  signer => sub {
      # some HMAC-SHA256 stuff
      # copied from https://metacpan.org/dist/WWW-OAuth/source/lib/WWW/OAuth.pm#L94-101
        my ($base_str, $client_secret, $token_secret) = @_;
        $token_secret = '' unless defined $token_secret;
        my $signing_key = uri_escape_utf8($client_secret) . '&' . uri_escape_utf8($token_secret);
        my $digest = hmac_sha256_base64($base_str, $signing_key);
        $digest .= '='x(4 - length($digest) % 4) if length($digest) % 4; # Digest::SHA does not pad Base64 digests
        return $digest;
        }
);

my $ua  = Mojo::UserAgent->new;
$ua->on(start => sub {
            $oauth->authenticate($_[1]->req,{realm=>$realm});
        });

say Dumper($oauth);

my $res = $ua->post($site => {'Prefer' => 'transient'} => json => {q => "select * from promotionCode where id = 250" },
                  )->result;

if    ($res->is_success)  { say "Success: ", $res->body }
elsif ($res->is_error)    { say "Error: ", $res->message }
elsif ($res->code == 301) { say "301: ", $res->headers->location }
else                      { say 'Whatever...' };


say "Response body: ", $res->body;


say "Getting invoice: ";

    my $invoiceurl = Mojo::URL->new($invoice_base_url);
    $invoiceurl->query({q => 'createdDate ON_OR_AFTER "5/02/2023"'});
    say "invoiceurl: $invoiceurl";
    $res = $ua->get($invoiceurl)->result;



if    ($res->is_success)  { say "Success: ", $res->body }
elsif ($res->is_error)    { say "Error: ", $res->message }
elsif ($res->code == 301) { say "301: ", $res->headers->location }
else                      { say 'Whatever...' };


say Dumper($res->json);
